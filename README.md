# The Simpsons: Tapped Out tool #

This project is under heavy development. A lot of stuff **will** be changed!

This tool allows you to add/modify stuff in your TSTO Springfield.
It propably violates EAs EULA and definitely their TOS, because it allows you
to give yourself :doughnut:, :dollar: and stuff...

**USE AT YOUR OWN RISK**

The `tsto.py` is heavily based on Oleg Polivets (jsbot@ya.ru) TSTO tool which
can be found on [github](https://github.com/schdub/tsto).
**Thanks** Oleg for your work.


## USAGE ##

Fire up `tsto_cli.py` and type `help`. You should see some help :D


## DPENDENCIES ##

Since these are python scripts you obviously need python. version 2.7 or 3.x
should work fine.

Additionally you need some python modules. Since most people are lazy you can
copy and paste the following line if you use `pip`

    pip install cmdln requests protobuf lxml


## TODO ##

*  implement more functions
*  get more stuff out of dlc-files
*  improve dlc file handling
*  improve interface

import os, sys, requests, logging

if not globals().has_key("CONFIGDIR"):
    CONFIGDIR = os.path.join(os.path.expanduser('~'),'.tsto')
    if not os.path.isdir(CONFIGDIR):
        os.mkdir(CONFIGDIR)

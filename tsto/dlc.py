#!/usr/bin/env python3
import logging, os, sys, requests
from lxml import etree
from io import BytesIO
from zipfile import ZipFile,BadZipfile
from . import CONFIGDIR

CACHEDIR = os.path.join(CONFIGDIR,"dlc")
if not os.path.isdir(CACHEDIR):
	os.mkdir(CACHEDIR)

DLCLocation = "http://oct2015-4-17-0-fj39afaed.tstodlc.eamobile.com/netstorage/gameasset/direct/simpsons/"

class DLCIndexZip(object):
	"""Get object information from DLCIndex-files

	Attributes:
		xml			squashed xml files found in the gamescripts file
	"""

	def __init__(self, DLCPath):
		"""DLCPath - path to dlc-zip relative from DLCLocation"""

		indexZip = _getZip(DLCPath)
		indexEtree = etree.fromstring(indexZip.read(indexZip.namelist()[0]))

		gsfile = next(node.get("val").replace(':','/') 
				for node in indexEtree.findall(".//FileName[@val]") 
				if node.get("val").find("gamescripts") > -1)
		
		self.xml = self._load_xml(gsfile)

	def _load_xml(self, filename):
		logging.info("Loading xml data")
		zip1 = _getZip(filename)
		xml = etree.Element("TSTOROOT")
		for xmlFile in zip1.namelist():
			if xmlFile[-4:] == '.xml':
				elm = etree.Element("File",{"name":xmlFile})
				elm.extend(etree.fromstring(zip1.read(xmlFile)))
				xml.append(elm)
		return xml

def get_latest():
	"""Returns DLCIndexZip of latest update"""
	dlcIndexZip = _getZip('dlc/DLCIndex.zip',True)

	dlcIndexXml = etree.fromstring(dlcIndexZip.read("DLCIndex.xml"))
	latestDlc = dlcIndexXml.findall("IndexFile")[0]

	logging.info("Latest DLC %s" % latestDlc.get('version'))
	return DLCIndexZip(latestDlc.get("index").replace(':','/'))

def _getZip(filename,force_download=False):
	"""Return ZipFile object of file or
	file 1 if it is in file"""
	cachefile = os.path.join(CACHEDIR, os.path.basename(filename))
	if not force_download and os.path.isfile(cachefile):
		print("Loading %s" % os.path.basename(filename))
	else:
		print("Downloading %s" % os.path.basename(filename))

		with open(cachefile,'wb') as f:
			f.write(requests.get(DLCLocation + filename).content)
	try:
		fileNameZip = ZipFile( cachefile, "r" )
	except BadZipfile as e:
		os.remove(cachefile)
		raise BadZipfile(e)

	if '1' in fileNameZip.namelist():
		return ZipFile( BytesIO( fileNameZip.read("1") ) )
	else:
		return fileNameZip

if __name__ == "__main__":
	logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.DEBUG)
	dlc = get_latest()
